import * as flsFunctions from "./modules/function.js";
flsFunctions.isWebp();

const checkbox = document.querySelector(".burger-menu__input"); 
const menu = document.querySelector(".menu");
const list = document.querySelector(".menu__list");
const items = document.querySelectorAll(".menu__list__item");
const headerMenuLinks = document.querySelectorAll(".menu__list__item__link");

checkbox.addEventListener('click', function () {
  if (checkbox.checked) {
    list.classList.add('active_list');
    items.forEach(elem => elem.classList.add('active_item'));
    headerMenuLinks.forEach(elem => elem.classList.add('active_link'));
    menu.id = 'active_menu';
  } else {
    list.classList.remove('active_list');
    items.forEach(elem => elem.classList.remove('active_item'));
    headerMenuLinks.forEach(elem => elem.classList.remove('active_link'));
    menu.id = '';
  }
});

document.body.addEventListener("click", function(event) {
  if (!menu.contains(event.target) && event.target !== checkbox) {
    menu.id = "";
    checkbox.checked = false;
    list.classList.remove("active_list");
    items.forEach(elem => elem.classList.remove("active_item"));
    headerMenuLinks.forEach(elem => elem.classList.remove("active_link"));
  }
});

const headerMainText = document.querySelector(".header__main__text");
const tablet = window.matchMedia("(min-width: 481px)");

if (tablet.matches) {
  headerMainText.innerText = "A real gamechanger in the world of web development";
}
